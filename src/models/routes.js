const express = require("express");
const ProductControllers = require("./controllers/ProductControllers");
const routes = express.Router();

routes.get("/", (req, res) => {

   res.send({status: "criando routes"})
});

routes.get("/product", ProductControllers.index);
routes.get("/product/:id", ProductControllers.show);
routes.post("/product", ProductControllers.store);
routes.put("/product/:id", ProductControllers.update);
routes.delete("/product/:id", ProductControllers.destroy);

module.exports = routes;