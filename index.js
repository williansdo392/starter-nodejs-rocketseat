const express = require("express");
const cors = require('cors');
const mongoose = require('mongoose');
const requireDir = require('require-dir');

const app = express();
app.use(express.json());
app.use(cors());

mongoose.connect(
    'mongodb://localhost:27017/starter-nodejs-rocketseat',
    { useNewUrlParser: true }
),

app.use('/api', require('./src/models/routes'));

app.listen (3000,()=>{console.log('server ativo na porta 3000')
});